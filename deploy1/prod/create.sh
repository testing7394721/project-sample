#!/bin/bash

usage() {
    echo "Usage: $0 --cluster CLUSTER_NAME --service SERVICE_NAME --task TASK_NAME --targetgroup TARGET_GROUP DOCKER_IMAGE"
    exit 1
}

while true ; do
    case "$1" in
        -t|--task) TASK_NAME=$2 ; shift 2 ;;
        -s|--service) SERVICE_NAME=$2 ; shift 2 ;;
        -c|--cluster) CLUSTER_NAME=$2 ; shift 2 ;;
        -tg|--targetgroup) TARGET_GROUP=$2 ; shift 2 ;;
        -h|--help) usage ;;

        --) shift ; break ;;
        *) break ;;
    esac
done

[ $# -eq 0 -o -z "$TASK_NAME" -o -z "$SERVICE_NAME" -o -z "$CLUSTER_NAME" -o -z "$TARGET_GROUP" ] && usage
DOCKER_IMAGE=$1
echo "$TASK_NAME $SERVICE_NAME $CLUSTER_NAME $TARGET_GROUP $DOCKER_IMAGE"
DEFAULT_TASK_DEF='{
    "family": "",
    "taskRoleArn": "",
    "networkMode": "bridge",
    "containerDefinitions": [
        {
            "name": "",
            "image": "",
            "cpu": 0,
            "memory": 10240,
            "memoryReservation": 1024,
            "portMappings": [
                {
                    "containerPort": 80,
                    "hostPort": 0,
                    "protocol": "tcp"
                }
            ],
            "ulimits": [
                {
                "softLimit": 512,
                "hardLimit": 10240,
                "name": "nofile"
                }
            ],
            "essential": true
        }
    ],
    "volumes": [
    ],
    "placementConstraints": [
    ]
}'

DEFAULT_SERVICE_DEF='{
    "cluster": "",
    "serviceName": "",
    "taskDefinition": "",
    "loadBalancers": [
        {
            "targetGroupArn": "",
            "containerName": "",
            "containerPort": 80
        }
    ],
    "desiredCount": 1,
    "role": "ecsServiceRole",
    "deploymentConfiguration": {
        "maximumPercent": 200,
        "minimumHealthyPercent": 50
    }
}'


NEW_TASK_DEF=$(echo $DEFAULT_TASK_DEF | jq --arg NDI $DOCKER_IMAGE '.containerDefinitions[0].image=$NDI')
NEW_TASK_DEF=$(echo $NEW_TASK_DEF | jq --arg NDI $TASK_NAME '.family=$NDI')
NEW_TASK_DEF=$(echo $NEW_TASK_DEF | jq --arg NDI $TASK_NAME '.containerDefinitions[0].name=$NDI')
DEFAULT_SERVICE_DEF=$(echo $DEFAULT_SERVICE_DEF | jq --arg NDI $TASK_NAME '.taskDefinition=$NDI')
DEFAULT_SERVICE_DEF=$(echo $DEFAULT_SERVICE_DEF | jq --arg NDI $SERVICE_NAME '.serviceName=$NDI')
DEFAULT_SERVICE_DEF=$(echo $DEFAULT_SERVICE_DEF | jq --arg NDI $CLUSTER_NAME '.cluster=$NDI')
DEFAULT_SERVICE_DEF=$(echo $DEFAULT_SERVICE_DEF | jq --arg NDI $TARGET_GROUP '.loadBalancers[0].targetGroupArn=$NDI')
DEFAULT_SERVICE_DEF=$(echo $DEFAULT_SERVICE_DEF | jq --arg NDI $TASK_NAME '.loadBalancers[0].containerName=$NDI')
aws ecs register-task-definition --family $TASK_NAME --cli-input-json "$(echo $NEW_TASK_DEF)"
aws ecs create-service --cli-input-json "$(echo $DEFAULT_SERVICE_DEF)"
