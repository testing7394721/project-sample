usage() {
    echo "Usage: $0 --cluster CLUSTER_NAME --service SERVICE_NAME --task TASK_NAME --targetgroup TARGET_GROUP DOCKER_IMAGE"
    exit 1
}

while true ; do
    case "$1" in
        -t|--task) TASK_NAME=$2 ; shift 2 ;;
        -s|--service) SERVICE_NAME=$2 ; shift 2 ;;
        -c|--cluster) CLUSTER_NAME=$2 ; shift 2 ;;
        -tg|--targetgroup) TARGET_GROUP=$2 ; shift 2 ;;
        -h|--help) usage ;;
        --) shift ; break ;;
        *) break ;;
    esac
done

[ $# -eq 0 -o -z "$TASK_NAME" -o -z "$SERVICE_NAME" -o -z "$CLUSTER_NAME" -o -z "$TARGET_GROUP" ] && usage

DOCKER_IMAGE=$1

expr='.serviceArns[]|select(contains("/'$SERVICE_NAME'"))|split("/")|.[1]'
SNAME=$(aws ecs list-services --output json --cluster $CLUSTER_NAME | jq -r $expr)
if [[ -z "$SNAME" ]]; then
    deploy1/prod/create.sh --cluster $CLUSTER_NAME --service $SERVICE_NAME --task $TASK_NAME --targetgroup $TARGET_GROUP $DOCKER_IMAGE
else
    deploy1/prod/update.sh --cluster $CLUSTER_NAME --service $SERVICE_NAME --task $TASK_NAME $DOCKER_IMAGE
fi
