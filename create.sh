#!/bin/bash

usage() {
    echo "Usage: $0 --cluster PREPROD_CLUSTER_NAME --service PREPROD_SERVICE_NAME --task PREPROD_TASK_NAME --targetgroup PREPROD_TARGET_GROUP DOCKER_IMAGE"
    exit 1
}

while true ; do
    case "$1" in
        -t|--task) PREPROD_TASK_NAME=$2 ; shift 2 ;;
        -s|--service) PREPROD_SERVICE_NAME=$2 ; shift 2 ;;
        -c|--cluster) PREPROD_CLUSTER_NAME=$2 ; shift 2 ;;
        -tg|--targetgroup) PREPROD_TARGET_GROUP=$2 ; shift 2 ;;
        -h|--help) usage ;;

        --) shift ; break ;;
        *) break ;;
    esac
done

[ $# -eq 0 -o -z "$PREPROD_TASK_NAME" -o -z "$PREPROD_SERVICE_NAME" -o -z "$PREPROD_CLUSTER_NAME" -o -z "$PREPROD_TARGET_GROUP" ] && usage
DOCKER_IMAGE=$1
echo "$PREPROD_TASK_NAME $PREPROD_SERVICE_NAME $PREPROD_CLUSTER_NAME $PREPROD_TARGET_GROUP $DOCKER_IMAGE"
DEFAULT_TASK_DEF='{
    "family": "",
    "taskRoleArn": "",
    "networkMode": "bridge",
    "containerDefinitions": [
        {
            "name": "",
            "image": "",
            "cpu": 256,
            "memory": 512,
            "portMappings": [
                {
                    "containerPort": 5000,
                    "hostPort": 5000,
                    "protocol": "tcp"
                }
            ],
            "essential": true

        }
    ],
    "volumes": [
    ],
    "placementConstraints": [
    ]
}'

DEFAULT_SERVICE_DEF='{
    "cluster": "",
    "serviceName": "",
    "taskDefinition": "",
    "loadBalancers": [
        {
            "targetGroupArn": "",
            "containerName": "",
            "containerPort": 5000
        }
    ],
    "desiredCount": 2,
    "role": "",
    "deploymentConfiguration": {
        "maximumPercent": 200,
        "minimumHealthyPercent": 50
    }
}'


NEW_TASK_DEF=$(echo $DEFAULT_TASK_DEF | jq --arg NDI $DOCKER_IMAGE '.containerDefinitions[0].image=$NDI')
NEW_TASK_DEF=$(echo $NEW_TASK_DEF | jq --arg NDI $PREPROD_TASK_NAME '.family=$NDI')
NEW_TASK_DEF=$(echo $NEW_TASK_DEF | jq --arg NDI $PREPROD_TASK_NAME '.containerDefinitions[0].name=$NDI')
DEFAULT_SERVICE_DEF=$(echo $DEFAULT_SERVICE_DEF | jq --arg NDI $PREPROD_TASK_NAME '.taskDefinition=$NDI')
DEFAULT_SERVICE_DEF=$(echo $DEFAULT_SERVICE_DEF | jq --arg NDI $PREPROD_SERVICE_NAME '.serviceName=$NDI')
DEFAULT_SERVICE_DEF=$(echo $DEFAULT_SERVICE_DEF | jq --arg NDI $PREPROD_CLUSTER_NAME '.cluster=$NDI')
DEFAULT_SERVICE_DEF=$(echo $DEFAULT_SERVICE_DEF | jq --arg NDI $PREPROD_TARGET_GROUP '.loadBalancers[0].targetGroupArn=$NDI')
DEFAULT_SERVICE_DEF=$(echo $DEFAULT_SERVICE_DEF | jq --arg NDI $PREPROD_TASK_NAME '.loadBalancers[0].containerName=$NDI')
aws ecs register-task-definition --family $PREPROD_TASK_NAME --cli-input-json "$(echo $NEW_TASK_DEF)"
aws ecs create-service --cli-input-json "$(echo $DEFAULT_SERVICE_DEF)"
