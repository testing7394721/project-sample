wget $DEV_PEM_FILE_PATH
# Make the file as private
chmod 400 $DEV_PEM_FILE

# Connect to dev server
ssh -i $DEV_PEM_FILE  -o StrictHostKeyChecking=no -o UserKnownHostsFile=/chat/null ubuntu@$DEV_SERVER << EOF

# Remove previous containers
docker rm -f gameroompod-chat

docker pull $CI_REGISTRY_IMAGE:chat-"$CI_PIPELINE_ID"

# Download the image from registry and contenarize it
docker run -p $CHAT_SERVER_PORT:$DOCKER_PORT --add-host=$DEV_HOST:$DEV_SERVER --name gameroompod-chat -d $CI_REGISTRY_IMAGE:chat-"$CI_PIPELINE_ID"

EOF
