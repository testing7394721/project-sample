#wget $STAGE_PEM_FILE_PATH
# Make the file as private
chmod 400 $PROD_PEM_FILE

#here we need to specify pem file and container name and prod_server details in environment variables 

# Connect to dev server
ssh -o StrictHostKeyChecking=no -i $PROD_PEM_FILE ubuntu@$PROD_SERVER << EOF
#before removing previous containers , first we should install docker on target machine
# add the below command for add ubuntu user into docker group for non tty session 
# sudo usermod -aG docker $USER (execute below command on target machine)

# Remove previous containers

docker rm -f $CONTAINER_NAME

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

docker pull $CI_REGISTRY_IMAGE:dev-"$CI_PIPELINE_ID"

# Download the image from registry and contenarize it
docker run -p 3000:3000 --add-host=nodejs:$PROD_SERVER --name $CONTAINER_NAME -d $CI_REGISTRY_IMAGE:dev-"$CI_PIPELINE_ID"

EOF
