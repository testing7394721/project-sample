usage() {
    echo "Usage: $0 --cluster DEV_CLUSTER_NAME --service DEV_SERVICE_NAME --task DEV_TASK_NAME --targetgroup DEV_TARGET_GROUP DOCKER_IMAGE"
    exit 1
}

while true ; do
    case "$1" in
        -t|--task) DEV_TASK_NAME=$2 ; shift 2 ;;
        -s|--service) DEV_SERVICE_NAME=$2 ; shift 2 ;;
        -c|--cluster) DEV_CLUSTER_NAME=$2 ; shift 2 ;;
        -tg|--targetgroup) DEV_TARGET_GROUP=$2 ; shift 2 ;;
        -h|--help) usage ;;
        --) shift ; break ;;
        *) break ;;
    esac
done

[ $# -eq 0 -o -z "$DEV_TASK_NAME" -o -z "$DEV_SERVICE_NAME" -o -z "$DEV_CLUSTER_NAME" -o -z "$DEV_TARGET_GROUP" ] && usage

DOCKER_IMAGE=$1

expr='.serviceArns[]|select(contains("/'$DEV_SERVICE_NAME'"))|split("/")|.[1]'
SNAME=$(aws ecs list-services --output json --cluster $DEV_CLUSTER_NAME | jq -r $expr)
if [[ -z "$SNAME" ]]; then
    deploy/aws/create.sh --cluster $DEV_CLUSTER_NAME --service $DEV_SERVICE_NAME --task $DEV_TASK_NAME --targetgroup $DEV_TARGET_GROUP $DOCKER_IMAGE
else
    deploy/aws/update.sh --cluster $DEV_CLUSTER_NAME --service $DEV_SERVICE_NAME --task $DEV_TASK_NAME $DOCKER_IMAGE
fi

