#!/bin/bash

usage() {
    echo "Usage: $0 --cluster DEV_CLUSTER_NAME --service DEV_SERVICE_NAME --task DEV_TASK_NAME --targetgroup DEV_TARGET_GROUP DOCKER_IMAGE"
    exit 1
}

while true ; do
    case "$1" in
        -t|--task) DEV_TASK_NAME=$2 ; shift 2 ;;
        -s|--service) DEV_SERVICE_NAME=$2 ; shift 2 ;;
        -c|--cluster) DEV_CLUSTER_NAME=$2 ; shift 2 ;;
        -tg|--targetgroup) DEV_TARGET_GROUP=$2 ; shift 2 ;;
        -h|--help) usage ;;

        --) shift ; break ;;
        *) break ;;
    esac
done

[ $# -eq 0 -o -z "$DEV_TASK_NAME" -o -z "$DEV_SERVICE_NAME" -o -z "$DEV_CLUSTER_NAME" -o -z "$DEV_TARGET_GROUP" ] && usage
DOCKER_IMAGE=$1
echo "$DEV_TASK_NAME $DEV_SERVICE_NAME $DEV_CLUSTER_NAME $DEV_TARGET_GROUP $DOCKER_IMAGE"
DEFAULT_TASK_DEF='{
    "family": "",
    "taskRoleArn": "",
    "networkMode": "bridge",
    "containerDefinitions": [
        {
            "name": "",
            "image": "",
            "cpu": 0,
            "memory": 1024,
            "memoryReservation": 1024,
            "portMappings": [
                {
                    "containerPort": 9055,
                    "hostPort": 0,
                    "protocol": "tcp"
                }
            ],
            "essential": true,
		"environment": [
		    {
		      "name": "SPRING_PROFILES_ACTIVE",
		      "value": "default"
		    }
		]

        }
    ],
    "volumes": [
    ],
    "placementConstraints": [
    ]
}'

DEFAULT_SERVICE_DEF='{
    "cluster": "",
    "serviceName": "",
    "taskDefinition": "",
    "loadBalancers": [
        {
            "targetGroupArn": "",
            "containerName": "",
            "containerPort": 9055
        }
    ],
    "desiredCount": 1,
    "role": "ecsServiceRole",
    "deploymentConfiguration": {
        "maximumPercent": 200,
        "minimumHealthyPercent": 50
    }
}'


NEW_TASK_DEF=$(echo $DEFAULT_TASK_DEF | jq --arg NDI $DOCKER_IMAGE '.containerDefinitions[0].image=$NDI')
NEW_TASK_DEF=$(echo $NEW_TASK_DEF | jq --arg NDI $DEV_TASK_NAME '.family=$NDI')
NEW_TASK_DEF=$(echo $NEW_TASK_DEF | jq --arg NDI $DEV_TASK_NAME '.containerDefinitions[0].name=$NDI')
DEFAULT_SERVICE_DEF=$(echo $DEFAULT_SERVICE_DEF | jq --arg NDI $DEV_TASK_NAME '.taskDefinition=$NDI')
DEFAULT_SERVICE_DEF=$(echo $DEFAULT_SERVICE_DEF | jq --arg NDI $DEV_SERVICE_NAME '.serviceName=$NDI')
DEFAULT_SERVICE_DEF=$(echo $DEFAULT_SERVICE_DEF | jq --arg NDI $DEV_CLUSTER_NAME '.cluster=$NDI')
DEFAULT_SERVICE_DEF=$(echo $DEFAULT_SERVICE_DEF | jq --arg NDI $DEV_TARGET_GROUP '.loadBalancers[0].targetGroupArn=$NDI')
DEFAULT_SERVICE_DEF=$(echo $DEFAULT_SERVICE_DEF | jq --arg NDI $DEV_TASK_NAME '.loadBalancers[0].containerName=$NDI')
aws ecs register-task-definition --family $DEV_TASK_NAME --cli-input-json "$(echo $NEW_TASK_DEF)"
aws ecs create-service --cli-input-json "$(echo $DEFAULT_SERVICE_DEF)"
