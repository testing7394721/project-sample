#!/bin/bash
usage() {
    echo "Usage: $0 --cluster CLUSTER_NAME --service SERVICE_NAME --task TASK_NAME DOCKER_IMAGE"
    exit 1
}

while true ; do
    case "$1" in
        -t|--task) TASK_NAME=$2 ; shift 2 ;;
        -s|--service) SERVICE_NAME=$2 ; shift 2 ;;
        -c|--cluster) CLUSTER_NAME=$2 ; shift 2 ;;
        -h|--help) usage ;;
        --) shift ; break ;;
        *) break ;;
    esac
done

[ $# -eq 0 -o -z "$TASK_NAME" -o -z "$SERVICE_NAME" -o -z "$CLUSTER_NAME"  ] && usage

DOCKER_IMAGE=$1
# Get the current service running
SERVICE_DEF=$(aws ecs describe-services --service $SERVICE_NAME --cluster $CLUSTER_NAME --output json)
# Parse the task definition from it, outputs > "arn:aws:ecs:us-east-1:994390759806:task-definition/paninififa-trade:11"
SERVICE_TASK_DEF=$(echo $SERVICE_DEF | jq '.services[0].taskDefinition')
# Split the string after '/' character as the task description fails if the entire arn is given.
SERVICE_TASK_DEF=${SERVICE_TASK_DEF##*/}
# Removes laster character in the string, as after split the string is something like 'trade:8"'.
SERVICE_TASK_DEF=${SERVICE_TASK_DEF%?}
# Get the previous task definition
OLD_TASK_DEF=$(aws ecs describe-task-definition --task-definition $SERVICE_TASK_DEF --output json)
# Replace the image path with the current one.
TASK_DEF=$(echo $OLD_TASK_DEF | jq --arg NDI $DOCKER_IMAGE '.taskDefinition.containerDefinitions[0].image=$NDI')
# # Get the port of current task
PORT=$(echo $TASK_DEF | jq '.taskDefinition.containerDefinitions[0].portMappings[0].hostPort')
# If condition to shift ports, New task defination cannot be run on same port
#if [[ $PORT == 80 ]]
#then
#    PORT=180
#else
#    PORT=80
#fi
PORT=0
# # Replace the port in the task definition
TASK_DEF=$(echo $TASK_DEF | jq --argjson NDI $PORT '.taskDefinition.containerDefinitions[0].portMappings[0].hostPort=$NDI')
# Creates new task definition taking the family, volumes and contailerDef values from TASK_DEF
FINAL_TASK_DEF=$(echo $TASK_DEF | jq '.taskDefinition|{family: .family, volumes: .volumes, containerDefinitions: .containerDefinitions}')
aws ecs register-task-definition --family $TASK_NAME --cli-input-json "$(echo $FINAL_TASK_DEF)"
aws ecs update-service --service $SERVICE_NAME --task-definition $TASK_NAME --cluster $CLUSTER_NAME
