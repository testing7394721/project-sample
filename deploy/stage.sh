wget $STAGE_PEM_FILE_PATH
# Make the file as private
chmod 400 $STAGE_PEM_FILE

# Connect to dev server
ssh -i $STAGE_PEM_FILE  -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null ubuntu@$STAGE_SERVER << EOF

# Remove previous containers
docker rm -f gameroompod-pre-prod

docker pull $CI_REGISTRY_IMAGE:pre-prod-"$CI_PIPELINE_ID"

# Download the image from registry and contenarize it
docker run -p $PRE_PROD_SERVER_PORT:$DOCKER_PORT --add-host=$DEV_HOST:$DEV_SERVER --name gameroompod-pre-prod -d $CI_REGISTRY_IMAGE:pre-prod-"$CI_PIPELINE_ID"

EOF
