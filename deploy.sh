usage() {
    echo "Usage: $0 --cluster PREPROD_CLUSTER_NAME --service PREPROD_SERVICE_NAME --task PREPROD_TASK_NAME --targetgroup PREPROD_TARGET_GROUP DOCKER_IMAGE"
    exit 1
}

while true ; do
    case "$1" in
        -t|--task) PREPROD_TASK_NAME=$2 ; shift 2 ;;
        -s|--service) PREPROD_SERVICE_NAME=$2 ; shift 2 ;;
        -c|--cluster) PREPROD_CLUSTER_NAME=$2 ; shift 2 ;;
        -tg|--targetgroup) PREPROD_TARGET_GROUP=$2 ; shift 2 ;;
        -h|--help) usage ;;
        --) shift ; break ;;
        *) break ;;
    esac
done

[ $# -eq 0 -o -z "$PREPROD_TASK_NAME" -o -z "$PREPROD_SERVICE_NAME" -o -z "$PREPROD_CLUSTER_NAME" -o -z "$PREPROD_TARGET_GROUP" ] && usage

DOCKER_IMAGE=$1

expr='.serviceArns[]|select(contains("/'$PREPROD_SERVICE_NAME'"))|split("/")|.[1]'
SNAME=$(aws ecs list-services --output json --cluster $PREPROD_CLUSTER_NAME | jq -r $expr)
if [[ -z "$SNAME" ]]; then
    sh create.sh --cluster $PREPROD_CLUSTER_NAME --service $PREPROD_SERVICE_NAME --task $PREPROD_TASK_NAME --targetgroup $PREPROD_TARGET_GROUP $DOCKER_IMAGE
else
    sh update.sh --cluster $PREPROD_CLUSTER_NAME --service $PREPROD_SERVICE_NAME --task $PREPROD_TASK_NAME $DOCKER_IMAGE
fi

